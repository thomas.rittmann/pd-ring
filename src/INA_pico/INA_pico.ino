/***************************************************************************
* Example sketch for the INA219_WE library
*
* This sketch shows how to use the INA219 module in continuous mode. 
*  
* Further information can be found on:
* https://wolles-elektronikkiste.de/ina219 (German)
* https://wolles-elektronikkiste.de/en/ina219-current-and-power-sensor (English)
* 
***************************************************************************/
#include <Wire.h>
#include <INA219_WE.h>
#define I2C_ADDRESS 0x40

/* There are several ways to create your INA219 object:
 * INA219_WE ina219 = INA219_WE()              -> uses Wire / I2C Address = 0x40
 * INA219_WE ina219 = INA219_WE(ICM20948_ADDR) -> uses Wire / I2C_ADDRESS
 * INA219_WE ina219 = INA219_WE(&wire2)        -> uses the TwoWire object wire2 / I2C_ADDRESS
 * INA219_WE ina219 = INA219_WE(&wire2, I2C_ADDRESS) -> all together
 * Successfully tested with two I2C busses on an ESP32
 */
INA219_WE ina219 = INA219_WE(I2C_ADDRESS);
int loopcount=0;

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(18, OUTPUT);
  Serial.begin(115200);
  Wire.setSDA(0);
  Wire.setSCL(1);
  Wire.begin();
  if(!ina219.init()){
    Serial.println("INA219 not connected!");
  }

  Serial.println("INA219 Current Sensor Example Sketch - Continuous");  
  ina219.setADCMode(SAMPLE_MODE_4);
   ina219.setBusRange(BRNG_16); // choose range and uncomment for change of default
   ina219.setPGain(PG_40); // choose gain and uncomment for change of default
}

float current_mA_avg=0.0;
float cursum=0.0;
void loop() {

  loopcount ++;
  //Serial.print("Loop");
  //Serial.println(loopcount);
  digitalWrite(LED_BUILTIN, HIGH);
  digitalWrite(18, HIGH);
  delayMicroseconds(2000);

  
  float shuntVoltage_mV = 0.0;
  float loadVoltage_V = 0.0;
  float busVoltage_V = 0.0;
  float current_mA = 0.0;
  float power_mW = 0.0; 
  bool ina219_overflow = false;
  
  shuntVoltage_mV = ina219.getShuntVoltage_mV();
  busVoltage_V = ina219.getBusVoltage_V();
  current_mA = ina219.getCurrent_mA()/10 +0.01;
  power_mW = ina219.getBusPower();
  loadVoltage_V  = busVoltage_V + (shuntVoltage_mV/1000);
  ina219_overflow = ina219.getOverflow();

  cursum=cursum+current_mA;
  if ((loopcount % 1000)==0)
  {
   current_mA_avg=cursum/1000;
   cursum=0;
 }
  //Serial.print("Shunt Voltage [mV]: "); Serial.println(shuntVoltage_mV);
  //Serial.print("Bus Voltage [V]: "); Serial.println(busVoltage_V);
  //Serial.print("Load Voltage [V]: "); Serial.println(loadVoltage_V);
 Serial.print(current_mA);
 Serial.print(", ");
 Serial.println(current_mA_avg);
  //Serial.print("Bus Power [mW]: "); Serial.println(power_mW);
  if(!ina219_overflow){
    
  }
  else{
    Serial.println("Overflow! Choose higher PGAIN");
  }
  
  digitalWrite(18, LOW);
  digitalWrite(LED_BUILTIN, LOW);
  delayMicroseconds(1000);
}
