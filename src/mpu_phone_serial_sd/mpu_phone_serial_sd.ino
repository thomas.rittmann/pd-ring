// Basic demo for accelerometer readings from Adafruit MPU6050

#include <Adafruit_MPU6050.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_AHRS_Mahony.h>
#include <bluefruit.h>
#include <Wire.h>
#include <Adafruit_DotStar.h>
#include <SPI.h>
#include <SD.h>

// There is only one pixel on the board
#define NUMPIXELS 1 

//Use these pin definitions for the ItsyBitsy M4
#define DATAPIN    8
#define CLOCKPIN   6

Adafruit_MPU6050 mpu;
Adafruit_Mahony filter;
Adafruit_DotStar strip(NUMPIXELS, DATAPIN, CLOCKPIN, DOTSTAR_BGR);


BLEDfu bledfu;

// Uart over BLE service
BLEUart bleuart;

// Function prototypes for packetparser.cpp
uint8_t readPacket (BLEUart *ble_uart, uint16_t timeout);
float   parsefloat (uint8_t *buffer);
void    printHex   (const uint8_t * data, const uint32_t numBytes);
int loopcount=0;

// Packet buffer
extern uint8_t packetbuffer[];



float gyro_zero_offsets[3] = {0,0,0};

const int chipSelect = 2;
const int sdblockpin=PIN_A4;
boolean sdfree;


void setup(void) {
 
  Serial.begin(115200);
  while (!Serial) {
    delay(10); // will pause Zero, Leonardo, etc until serial console opens
  }

  Serial.println("Connecting to MPU6050 chip...");
  Serial.flush();
  // Try to initialize!
  while (!mpu.begin()) {
    Serial.print(".");
    delay(50);
  }
  Serial.println("start gyroscope filter...");
  filter.begin(20);
  delay(10);
  mpu.setAccelerometerRange(MPU6050_RANGE_8_G);
  mpu.setGyroRange(MPU6050_RANGE_500_DEG);
  mpu.setFilterBandwidth(MPU6050_BAND_21_HZ);

  Serial.print("Initializing SD card...");
  Serial.flush();
  //SdFile::dateTimeCallback(dateTime);
  // see if the card is present and can be initialized:
  if (!SD.begin(chipSelect)) {
    Serial.println("Card failed, or not present");
    delay(200);
    // don't do anything more:
    while (1);
  }
  Serial.println("card initialized.");
  
  Serial.println("start Bluetooth advertising...");
  delay(100);
  Bluefruit.configPrphBandwidth(BANDWIDTH_MAX);  
  Bluefruit.begin();
  Bluefruit.setTxPower(8);    // Check bluefruit.h for supported values
  Bluefruit.setName("PD-Ring");
  Bluefruit.Periph.setConnInterval(6, 12); // 7.5 - 15 ms

  // To be consistent OTA DFU should be added first if it exists
  bledfu.begin();

  // Configure and start the BLE Uart service
  bleuart.begin();

  // Set up and start advertising
  startAdv();
  Serial.println("");

  pinMode(sdblockpin, INPUT);    // sets the digital pin 7 as input
  digitalWrite(sdblockpin,LOW);
  
  strip.begin(); // Initialize pins for output
  strip.setBrightness(100);
  strip.setPixelColor(0,255,0,0);
  strip.show(); 
}

void loop() {
   loopcount++;
  /* Get new sensor events with the readings */
  sensors_event_t a, g, temp;
  mpu.getEvent(&a, &g, &temp);

  filter.update(
    g.gyro.x,
    g.gyro.y,
    g.gyro.z,
    a.acceleration.x,a.acceleration.y,a.acceleration.z,             
                 0,0,0);

    float qw, qx, qy, qz;
    filter.getQuaternion(&qw, &qx, &qy, &qz);
  /* Print out the values */

  //char serialbuf[64];
  //sprintf(serialbuf,"%.3f,%.3f,%.3f, %.3f,%.3f,%.3f",a.acceleration.x,a.acceleration.y,a.acceleration.z,g.gyro.x,g.gyro.y,g.gyro.z);
  //Serial.println(serialbuf);
  char fullbuf[64];
  sprintf(fullbuf,"%lu, %.3f,%.3f,%.3f, %.3f,%.3f,%.3f",a.timestamp,a.acceleration.x,a.acceleration.y,a.acceleration.z,g.gyro.x,g.gyro.y,g.gyro.z);
if (loopcount>5)
{
char prefix[] = "V";
    bleuart.write(prefix, strlen(prefix));
    bleuart.write((uint8_t *)&qw, sizeof(float));
    bleuart.write((uint8_t *)&qx, sizeof(float));
    bleuart.write((uint8_t *)&qy, sizeof(float));
    bleuart.write((uint8_t *)&qz, sizeof(float));

  loopcount=0;
}


  digitalWrite(sdblockpin,LOW);
  sdfree = digitalRead(sdblockpin);   // read the input pin
  if (!sdfree){
    strip.setBrightness(100);
    strip.show();
    // ATTENTION: FILENAME is in 8.3 format: it can have maximally 8 characters before the DOT and max. 3 for file extension. 
    File dataFile = SD.open("gyrofu.txt", FILE_WRITE);
    // if the file is available, write to it:
    if (dataFile) {
      dataFile.println(fullbuf);
      dataFile.close();
      // print to the serial port too:
      //Serial.println(dataString);
    }
    // if the file isn't open, pop up an error:
    else {
      Serial.println("error opening datalog.txt");
    }
    
    strip.setBrightness(0);
    strip.show();
  }
  else{
    Serial.println("SD Write was blocked by user to enable safe shutdown");
  }

delay(2);
}





void startAdv(void)
{
  // Advertising packet
  Bluefruit.Advertising.addFlags(BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE);
  Bluefruit.Advertising.addTxPower();
  
  // Include the BLE UART (AKA 'NUS') 128-bit UUID
  Bluefruit.Advertising.addService(bleuart);

  // Secondary Scan Response packet (optional)
  // Since there is no room for 'Name' in Advertising packet
  Bluefruit.ScanResponse.addName();

  /* Start Advertising
   * - Enable auto advertising if disconnected
   * - Interval:  fast mode = 20 ms, slow mode = 152.5 ms
   * - Timeout for fast mode is 30 seconds
   * - Start(timeout) with timeout = 0 will advertise forever (until connected)
   * 
   * For recommended advertising interval
   * https://developer.apple.com/library/content/qa/qa1931/_index.html   
   */
  Bluefruit.Advertising.restartOnDisconnect(true);
  Bluefruit.Advertising.setInterval(32, 244);    // in unit of 0.625 ms
  Bluefruit.Advertising.setFastTimeout(30);      // number of seconds in fast mode
  Bluefruit.Advertising.start(0);                // 0 = Don't stop advertising after n seconds  
}
