// Basic demo for accelerometer readings from Adafruit MPU6050

#include <Adafruit_MPU6050.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_AHRS_Mahony.h>
#include <bluefruit.h>
#include <Wire.h>
#include <Adafruit_DotStar.h>
#include <SPI.h>
#include <SD.h>

// There is only one pixel on the board
#define NUMPIXELS 1 

//Use these pin definitions for the ItsyBitsy M4
#define DATAPIN    8
#define CLOCKPIN   6
#define NCARDBUF 30
#define NBAT 20
#define BATSAMPLING 10000
Adafruit_MPU6050 mpu;

BLEDfu bledfu;

// Uart over BLE service
BLEUart bleuart;
File dataFile;
File batFile;

// Function prototypes for packetparser.cpp
uint8_t readPacket (BLEUart *ble_uart, uint16_t timeout);
float   parsefloat (uint8_t *buffer);
void    printHex   (const uint8_t * data, const uint32_t numBytes);
int loopcount=0;

// Packet buffer
extern uint8_t packetbuffer[];

float gyro_zero_offsets[3] = {0,0,0};

const int chipSelect = 2;
const int sdblockpin=PIN_A4;
const int sleeppin=PIN_A3;
boolean sdfree;
char serialbuf[34];
char cardbuf[NCARDBUF][100];
unsigned long tbefore;
unsigned long passed;
unsigned long loopstamp;
unsigned long batterystamp=0;
int batcount=0;
int adcin    = PIN_A1;
int adcvalue = 0;
float mv_per_lsb = 3600.0F/1024.0F; // 10-bit ADC with 3.6V input range
float vbat_divider=330.0/(330.0+220.0);
float mvolt;
String gyrofilename="gyrodata.txt";

void setup(void) {
 
  //Serial.begin(115200);
  //while (!Serial) {
   // delay(10); // will pause Zero, Leonardo, etc until serial console opens
  //}

  Serial.println("Connecting to MPU6050 chip...");
  Serial.flush();
  // Try to initialize!
  while (!mpu.begin()) {
    Serial.print(".");
   delay(50);
  }
  //Serial.println("start gyroscope filter...");
  //filter.begin(20);
  //delay(10);
  mpu.setAccelerometerRange(MPU6050_RANGE_8_G);
  mpu.setGyroRange(MPU6050_RANGE_500_DEG);
  mpu.setFilterBandwidth(MPU6050_BAND_21_HZ);
  mpu.enableSleep(false);
  mpu.gyroStandby(true);
  //mpu.setCycleRate(MPU6050_CYCLE_1_25_HZ);
  //mpu.enableCycle(true);
  //mpu.setCycleRate(MPU6050_CYCLE_40_HZ);
 // Serial.print("Initializing SD card...");
 // Serial.flush();
//  //SdFile::dateTimeCallback(dateTime);
//  // see if the card is present and can be initialized:

  
  //Serial.println("start Bluetooth advertising...");
  delay(100);
  Bluefruit.autoConnLed(false);
  Bluefruit.configPrphBandwidth(BANDWIDTH_MAX);  
  Bluefruit.begin();
  Bluefruit.setTxPower(-8);    // Check bluefruit.h for supported values   -40; -20; -16; -12; -8; -4; 0; 4; 8(?)
  Bluefruit.setName("PD-Ring");
  Bluefruit.Periph.setConnInterval(6, 12); // 7.5 - 15 ms

  // To be consistent OTA DFU should be added first if it exists
  bledfu.begin();

  // Configure and start the BLE Uart service
  bleuart.begin();

  // Set up and start advertising
  startAdv();
  Serial.println("");

  pinMode(sdblockpin, INPUT);    // sets the digital pin 7 as input
  digitalWrite(sdblockpin,LOW);

  pinMode(sleeppin,INPUT_PULLUP);
  digitalWrite(sleeppin,HIGH);
  
    if (!SD.begin(chipSelect)) {
    //Serial.println("Card failed, or not present");
    delay(5000);
    //NRF_POWER->SYSTEMOFF = 1;
    // don't do anything more:
   while (1){
   delay(1000);}
   }
  
  Serial.println("card initialized.");
  dataFile = SD.open(gyrofilename, FILE_WRITE);
}

sensors_event_t a, g, temp;


void loop() {
  loopstamp=millis();
  /* Get new sensor events with the readings */
  sleepcheck();
  mpu.getEvent(&a, &g, &temp);

  sprintf(serialbuf,"%.3f,%.3f,%.3f",a.acceleration.x,a.acceleration.y,a.acceleration.z);
  bleuart.println(serialbuf);
  //Serial.println(serialbuf);
  //sprintf(cardbuf[loopcount],"%lu, %s",a.timestamp,serialbuf);
  sprintf(cardbuf[loopcount],"%lu,%s",loopstamp,serialbuf);

  writecheck();
  batterycheck();

 if((millis()-batterystamp)>2*BATSAMPLING)
 {
 dataFile.close();
 batterystamp=millis();
}
  //delay(1000);
  //artificially slowdown to max Hz
  while((millis()-loopstamp)<33) {
    delay(5);
    }
  //Serial.print("Loop duration: ");
  //Serial.println(millis()-loopstamp);
 }


boolean oldblock=1;


void writecheck(void)
{
    loopcount++;
  if (loopcount ==NCARDBUF){
    tbefore=millis();
    writetocard();
    passed=millis()-tbefore;
    Serial.print("Card T: ");
    Serial.println(passed);
    loopcount=0;
  }
}

void sleepcheck(void)
{
  if(!digitalRead(sleeppin)){
    mpu.enableSleep(true);
    NRF_POWER->SYSTEMOFF = 1;
  }
}

void batterycheck(void)
{
    if((millis()-batterystamp)>BATSAMPLING)
  {
    adcvalue += analogRead(adcin);
    batcount++;
    if (batcount==NBAT)
    {
      adcvalue/=NBAT;
      batcount=0;
      mvolt=adcvalue*mv_per_lsb/vbat_divider;
      batterystamp=millis();
  
      Serial.print("Battery: ");
      Serial.print(mvolt);
      Serial.print(" mV");
      digitalWrite(sdblockpin,LOW);
      sdfree = digitalRead(sdblockpin);   // read the input pin
      if (!sdfree){
      dataFile.close();
      batFile = SD.open("battery.txt", FILE_WRITE);
      batFile.print(millis());
      batFile.print(", ");
      batFile.println(mvolt);  
      batFile.close();
      Serial.println(" ...saved");
      }
      else
      {
        Serial.println(" ...not saved, block");
      }
      if(mvolt<3.7)
      {
         batFile = SD.open("battery.txt", FILE_WRITE);
         batFile.print("Critical Level reached - power down");
        batFile.close();
        if (dataFile)
        {
          dataFile.close();
        }
        NRF_POWER->SYSTEMOFF = 1;
      }
    }
  }
}

void writetocard(void)
{
  digitalWrite(sdblockpin,LOW);
  sdfree = digitalRead(sdblockpin);   // read the input pin
  if (!sdfree){
    // ATTENTION: FILENAME is in 8.3 format: it can have maximally 8 characters before the DOT and max. 3 for file extension. 
      if(!dataFile){
      dataFile = SD.open(gyrofilename, FILE_WRITE);
      } 
    // if the file is available, write to it:
    if (dataFile) {
      for (int i=0;i<NCARDBUF;i++)
      {
      dataFile.println(cardbuf[i]);
      }
      //dataFile.close();
      }
    else {
      Serial.println("error opening datalog.txt");
    }   
  }
  else{
    Serial.println("SD Write was blocked by user to enable safe shutdown");
  }
}


void startAdv(void)
{
  // Advertising packet
  Bluefruit.Advertising.addFlags(BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE);
  Bluefruit.Advertising.addTxPower();
  Bluefruit.Advertising.addService(bleuart);

  // Secondary Scan Response packet (optional)
  // Since there is no room for 'Name' in Advertising packet
  Bluefruit.ScanResponse.addName();

  /* Start Advertising
   * - Enable auto advertising if disconnected
   * - Interval:  fast mode = 20 ms, slow mode = 152.5 ms
   * - Timeout for fast mode is 30 seconds
   * - Start(timeout) with timeout = 0 will advertise forever (until connected)
   * 
   * For recommended advertising interval
   * https://developer.apple.com/library/content/qa/qa1931/_index.html   
   */
  Bluefruit.Advertising.restartOnDisconnect(true);
  //Bluefruit.Advertising.setInterval(32, 244);    // in unit of 0.625 ms
  Bluefruit.Advertising.setInterval(32, 1636);    // in unit of 0.625 ms
  Bluefruit.Advertising.setFastTimeout(5);      // number of seconds in fast mode
  Bluefruit.Advertising.start(0);                // 0 = Don't stop advertising after n seconds  
}
