/*********************************************************************
 This is an example for our nRF52 based Bluefruit LE modules

 Pick one up today in the adafruit shop!

 Adafruit invests time and resources providing this open source code,
 please support Adafruit and open-source hardware by purchasing
 products from Adafruit!

 MIT license, check LICENSE for more information
 All text above, and the splash screen below must be included in
 any redistribution
*********************************************************************/

#include <bluefruit.h>
#include <Adafruit_DotStar.h>
#include <SPI.h>
#include <SD.h>

// There is only one pixel on the board
#define NUMPIXELS 1 

//Use these pin definitions for the ItsyBitsy M4
#define DATAPIN    8
#define CLOCKPIN   6
Adafruit_DotStar strip(NUMPIXELS, DATAPIN, CLOCKPIN, DOTSTAR_BGR);
// OTA DFU service
BLEDfu bledfu;


const int chipSelect = 2;
// Uart over BLE service
BLEUart bleuart;

// Function prototypes for packetparser.cpp
uint8_t readPacket (BLEUart *ble_uart, uint16_t timeout);
float   parsefloat (uint8_t *buffer);
void    printHex   (const uint8_t * data, const uint32_t numBytes);
int loopcount=0;

// Packet buffer
extern uint8_t packetbuffer[];

unsigned int year = 2015;
byte month = 6;      
byte day = 18;
byte hour = 7;
byte minute = 8;
byte second = 9;

void dateTime(uint16_t* date, uint16_t* sdtime)
{
  *date = FAT_DATE(year, month, day);
  *sdtime = FAT_TIME(hour, minute, second);
}


void setup(void)
{
  strip.begin(); // Initialize pins for output
  strip.setBrightness(100);
  strip.setPixelColor(0,255,0,0);
  strip.show(); 
  Serial.begin(115200);
  while ( !Serial )  {delay(10);}   // for nrf52840 with native usb -- ONLY IF REALLY WANT TO WAIT FOR SERIAL
  Serial.println(F("Adafruit Bluefruit52 Controller App Example"));
  Serial.println(F("-------------------------------------------"));

  Bluefruit.autoConnLed(false);
  Bluefruit.begin();
  Bluefruit.setTxPower(-40);    // Check bluefruit.h for supported values
  Bluefruit.setName("PD-Ring");

  // To be consistent OTA DFU should be added first if it exists
  bledfu.begin();

  // Configure and start the BLE Uart service
  bleuart.begin();

  // Set up and start advertising
  startAdv();

  //Serial.println(F("Please use Adafruit Bluefruit LE app to connect in Controller mode"));
  //Serial.println(F("Then activate/use the sensors, color picker, game controller, etc!"));
  //Serial.println();  
  delay(100);
}

void startAdv(void)
{
  // Advertising packet
  Bluefruit.Advertising.addFlags(BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE);
  Bluefruit.Advertising.addTxPower();
  
  // Include the BLE UART (AKA 'NUS') 128-bit UUID
  Bluefruit.Advertising.addService(bleuart);

  // Secondary Scan Response packet (optional)
  // Since there is no room for 'Name' in Advertising packet
  Bluefruit.ScanResponse.addName();

  /* Start Advertising
   * - Enable auto advertising if disconnected
   * - Interval:  fast mode = 20 ms, slow mode = 152.5 ms
   * - Timeout for fast mode is 30 seconds
   * - Start(timeout) with timeout = 0 will advertise forever (until connected)
   * 
   * For recommended advertising interval
   * https://developer.apple.com/library/content/qa/qa1931/_index.html   
   */
  Bluefruit.Advertising.restartOnDisconnect(true);
  Bluefruit.Advertising.setInterval(32, 2056);    // in unit of 0.625 ms  it was 32,244
  Bluefruit.Advertising.setFastTimeout(10);      // number of seconds in fast mode  it was 30. 
  Bluefruit.Advertising.start(0);                // 0 = Don't stop advertising after n seconds  
}

/**************************************************************************/
/*!
    @brief  Constantly poll for new command or response data
*/
/**************************************************************************/
void loop(void)
{
  // Wait for new data to arrive
  Serial.println(F("---------"));
  loopcount++;
  Serial.println(loopcount);
  delay(1);

  uint8_t len = readPacket(&bleuart, 5);
  if (len == 0) return;
  if (packetbuffer[1] == 'C') {
    uint8_t red = packetbuffer[2];
    uint8_t green = packetbuffer[3];
    uint8_t blue = packetbuffer[4];
    strip.setPixelColor(0,red,green,blue);
    strip.show(); 
  }

}
