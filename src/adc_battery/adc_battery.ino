#include <Arduino.h>

int adcin    = PIN_A1;
int adcvalue = 0;
float mv_per_lsb = 3600.0F/1024.0F; // 10-bit ADC with 3.6V input range
float vbat_divider=330.0/(330.0+220.0);
float mvolt;
void setup() {
  Serial.begin(115200);
  while ( !Serial ) delay(10);   // for nrf52840 with native usb
}
char loopcounter=0;
void loop() {
  loopcounter++;
  // Get a fresh ADC value
  analogReadResolution(10); // Can be 8, 10, 12 or 14
  delay(1);
  adcvalue += analogRead(adcin);

  if (loopcounter>9){
  adcvalue/=10;
  mvolt=adcvalue*mv_per_lsb/vbat_divider;
  // Display the results
  Serial.print(adcvalue);
  Serial.print(" [");
  Serial.print(mvolt);
  Serial.print(" mV, ");
  Serial.print((float)mvToPercent((float)adcvalue*mv_per_lsb/vbat_divider));
  Serial.println("%]");
  adcvalue=0;
  loopcounter=0;
  delay(100);
  }
  delay(5);
}

uint8_t mvToPercent(float mvolts) {
  if(mvolts<3300)
    return 0;

  if(mvolts <3600) {
    mvolts -= 3300;
    return mvolts/30;
  }

  mvolts -= 3600;
  return 10 + (mvolts * 0.15F );  // thats mvolts /6.66666666
}
