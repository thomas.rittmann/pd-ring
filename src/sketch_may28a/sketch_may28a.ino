
int loopcount=0;
// Twinkle Twkinkle
void setup() {
pinMode(LED_BUILTIN, OUTPUT);
pinMode(18, OUTPUT);
Serial.begin(115200);
}

void loop()
{
  loopcount ++;
  Serial.print("Raspberry Pi Pico -- Loop");
  Serial.println(loopcount);
  digitalWrite(LED_BUILTIN, HIGH);
  digitalWrite(18, HIGH);
  delayMicroseconds(250000);
  digitalWrite(18, LOW);
  digitalWrite(LED_BUILTIN, LOW);
  delayMicroseconds(250000);
}
