t=linspace(0,36,10000);
omegac=40;
omega=omegac*ones([1,10000])+rand([1,10000])*0.01;
y = sin(omega.*t*2*pi).*(1+0.2*sin(3*omegac*t))+0.5+0.3.*sin(2*omega.*t*2*pi);
figure(22);
spectrogram(y,100,80,300,36,'yaxis');
ylim([0,12]);
set(gca,'clim',[-70,10]);
set(gca,'FontSize',13);