# PD Ring

Firmware for Parkinson monitoring ring. 
This project contains the code running on the ring's MCU (nRF52840). It is written in c++ and flashed to the chip using an Adafruit bootloader and the Arduino IDE. 