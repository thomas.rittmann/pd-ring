select_period=1738000:1896000;
Time_Select=Time(select_period);
%select axis: 
ax_Select=gy_x(select_period)+gy_y(select_period)+gy_z(select_period)+ax(select_period)+ay(select_period)+az(select_period);

time_cut=Time_Select(1:2^17);
ax_cut=ax_Select(1:2^17);

b1=2.611e7;
b2=2.664e7;

idx_block1=find(time_cut>2.611e7);
idx_block1_end=find(time_cut>2.631e7);
block1_idx=idx_block1(1):idx_block1(1)+2^12-1;

idx_block2=find(time_cut>2.664e7);
block2_idx=idx_block2(1):idx_block2(1)+2^11-1;

fig_timefull=figure(20);
    subplot(2,1,1);
    fac=1/60/1000;
    plot(time_cut*fac,ax_cut,'b');
    hold on;
    li(1)=line(time_cut(idx_block1(1))*[1,1]*fac,[-100,100]);
    li(2)=line(time_cut(idx_block1(1)+2^12-1)*fac*[1,1],[-100,100]);
    set(li,'Color','r');
    
    lib(1)=line(time_cut(idx_block2(1))*fac*[1,1],[-100,100]);
    lib(2)=line(time_cut(idx_block2(1)+2^11-1)*fac*[1,1],[-100,100]);
    set(lib,'Color','g');
    title('all');
    hold off;
    timest=(time_cut(end)-time_cut(1))/(2^17);
    freq=linspace(0,1000/timest,length(time_cut));
    ylim([-30,30]);
    subplot(2,1,2);
    spec = fft(ax_cut);
    plot(freq,smooth(abs(spec),100));
    xlim([0,8]);
    ylim([0,0.3e4]);
    


fig_restblock=figure(22);
    subplot(2,1,1);
    
    time_rest=time_cut(block1_idx);
    ax_rest=ax_cut(block1_idx);
    plot(time_rest,ax_rest);
    title('off');
    timest2=(time_rest(end)-time_rest(1))/(2^12);
    freq_rest=linspace(0,1000/timest2,length(time_rest));
    subplot(2,1,2);
    
    spec_rest = fft(ax_rest);
    plot(freq_rest,smooth(abs(spec_rest),5));
    xlim([0,12]);
    ylim([0,0.3e2]);
    
    
    
    

 fig_restblock=figure(23);
    subplot(2,1,1);
    time_rest=time_cut(block2_idx);
    ax_rest=ax_cut(block2_idx);
    plot(time_rest,ax_rest);
    title('on');
    timest2=(time_rest(end)-time_rest(1))/(2^11);
    freq_rest=linspace(0,1000/timest2,length(time_rest));
    subplot(2,1,2);
    spec_rest = fft(ax_rest);
    plot(freq_rest,smooth(abs(spec_rest),5));
    xlim([0,12]);
    ylim([0,20e2]);
    
    
