%% battery
load('CustomColormaps.mat');
bat_sec=10.68;
bat_time=(1:1:length(battery))*bat_sec/60/60;
figure(21);
plot(bat_time,battery/1e3);
xlabel('hours');
ylabel('battery voltage');
title('Battery (dis)charge profile');
%% data
select_period=1738000:1896000;
Time_Select=Time(select_period);

%% Time consistency
figure(12);
plot(diff(Time_Select),'.','MarkerSize',15);
xlim([0,1000]);
xlabel('Data Point #');
ylabel('Sampling Duration (ms)');
title('Sampling Rate Consistency');
%%
%select axis: 
ax_Select=1*gy_x(select_period)+1*gy_y(select_period)+1*gy_z(select_period)+1*ax(select_period)+1*ay(select_period)+1*az(select_period);

time_inter=[Time_Select(1):33:Time_Select(end)];
ax_inter=interp1(Time_Select,ax_Select,time_inter)';

time_cut=time_inter(1:2^17);
ax_cut=ax_inter(1:2^17);

%ime_cut=Time_Select(1:2^17);
%ax_cut=ax_Select(1:2^17);


b1=2.611e7;
b2=2.664e7;

idx_block1=find(time_cut>2.611e7);
idx_block1_end=find(time_cut>2.631e7);
block1_idx=idx_block1(1):idx_block1(1)+2^12-1;

idx_block2=find(time_cut>2.664e7);
block2_idx=idx_block2(1):idx_block2(1)+2^11-1;

fig_timefull=figure(20);
    subplot(2,1,1);
    fac=1/60/1000;
    plot(time_cut*fac,smooth(ax_cut,1)-smooth(ax_cut,30));
    hold on;
    li(1)=line(time_cut(idx_block1(1))*[1,1]*fac,[-100,100]);
    li(2)=line(time_cut(idx_block1(1)+2^12-1)*fac*[1,1],[-100,100]);
    set(li,'Color','r');
    xlim([min(time_cut*fac),max(time_cut*fac)]);
    xlabel('minutes');
    ylim([-60,60]);
    
    lib(1)=line(time_cut(idx_block2(1))*fac*[1,1],[-100,100]);
    lib(2)=line(time_cut(idx_block2(1)+2^11-1)*fac*[1,1],[-100,100]);
    set(lib,'Color','g');
    title('all');
    hold off;
    timest=(time_cut(end)-time_cut(1))/(2^17);
    freq=linspace(0,1000/timest,length(time_cut));
    %ylim([-60,60]);
    subplot(2,1,2);
    spec = fft(ax_cut);
    plot(freq,smooth(abs(spec),1000),'LineWidth',1.5);
    xlim([0,12]);
    ylim([0,0.3e4]);
    
% SPECTROGRAM
Nx=length(ax_cut);
nsc = floor(Nx/400.5);
nov = floor(nsc/2);
nff = max(512,2^nextpow2(nsc));
s=spectrogram(ax_cut,hamming(nsc),nov,nff);
asp=abs(s);
%view(-45,65)

asp=asp./sum(asp)*mean(sum(asp));
spectrofilt=imboxfilt(asp.^(1.6),[13,21]);
clim=([3,95]).^1.6*1./max(max(spectrofilt));

figure(73);
subplot(2,1,1);
    fac=1/60/1000;
    plot((time_cut-time_cut(1))*fac,smooth(ax_cut,1)-smooth(ax_cut,30));
    hold on;
    li(1)=line((time_cut(idx_block1(1))-time_cut(1))*[1,1]*fac,[-100,100]);
    li(2)=line((time_cut(idx_block1(1)+2^12-1)-time_cut(1))*fac*[1,1],[-100,100]);
    set(li,'Color','r');
    xlim([min((time_cut-time_cut(1))*fac),max((time_cut-time_cut(1))*fac)]);
    xlabel('Time (minutes)');
    ylim(max(abs(ax_cut))*[-0.8,0.8]);
    
    lib(1)=line((time_cut(idx_block2(1))-time_cut(1))*[1,1]*fac,[-100,100]);
    lib(2)=line((time_cut(idx_block2(1)+2^11-1)-time_cut(1))*fac*[1,1],[-100,100]);
    set(lib,'Color','g');
    title('Acceleration Amplitude');
    hold off;

subplot(2,1,2);
su=surf(linspace(0,(time_cut(end)-time_cut(1))/1000/60,size(spectrofilt,2)),linspace(0,15,257),spectrofilt./max(max(spectrofilt)),spectrofilt./max(max(spectrofilt))/1);
set(gca,'zlim',clim);
set(su,'EdgeColor','none');
set(gca,'clim',clim);
colormap(cmap_crosscor_w_b_r_y_HR);
set(gca,'YDir','normal');
xlabel('Time (minutes)');
ylabel('Tremor Frequency (Hz)');
cb=colorbar;
set(cb,'Position',[0.92,0.10,0.02,0.2]);
title('Tremor Spectrogram');
view(160,60)
%view(0,90) % flat image plot
ylim([0.7,10]);
xlim([0,75]);
%%figure(77);
%plot(sum(asp))


%imagesc(abs(speco));

%
    
 %

fig_activeblock=figure(22);
    subplot(2,2,1);
    
    time_rest=time_cut(block1_idx);
    ax_rest=ax_cut(block1_idx);
    hold off;
    plot((time_rest-time_rest(1))/1000,ax_rest-1*smooth(ax_rest,30));
    xlim([0,max(time_rest-time_rest(1))/1000]);
    title('Parkinson off');
    xlabel('Time (seconds)');
    ylabel('y-acceleration (m/s^2)');
    timest2=(time_rest(end)-time_rest(1))/(2^12);
    freq_rest=linspace(0,1000/timest2,length(time_rest));
    subplot(2,2,3);

    
    spec_rest = fft(ax_rest);
    plot(freq_rest,8*smooth(abs(spec_rest),20),'LineWidth',1.5);
    xlim([0,12]);
     ylim([-30,17e2]);
    xlabel('Frequency (Hz)');
    ylabel('Spectral Amplitude (a.u.)');
    
    
    

    subplot(2,2,2);
    time_rest=time_cut(block2_idx);
    ax_rest=ax_cut(block2_idx);
    plot((time_rest-time_rest(1))/1000,ax_rest-1*smooth(ax_rest,30));
    xlim([0,max(time_rest-time_rest(1))/1000]);
    xlabel('Time (seconds)');
    ylabel('y-acceleration (m/s^2)');
    title('Parkinson on');
    timest2=(time_rest(end)-time_rest(1))/(2^11);
    freq_rest=linspace(0,500/timest2,length(time_rest)/2);
    subplot(2,2,4);
    spec_rest = fftshift(fft(ax_rest));
    spec_rest=spec_rest(length(spec_rest)/2+1:length(spec_rest));
    plot(freq_rest,smooth(abs(spec_rest),30),'LineWidth',1.5);
    xlim([0,12]);
    ylim([-30,17e2]);
    xlabel('Frequency (Hz)');
    ylabel('Spectral Amplitude (a.u.)');
    
    
